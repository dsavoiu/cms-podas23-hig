#!/bin/sh
action () {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"

    source ${this_dir}/common.sh

    args=(
        --config $my_config
        --version $my_version
        --processes $my_sig_process,$my_bkg_process
        --datasets $my_sig_dataset,$my_bkg_dataset
        --variables jet1_pt,jet1_eta
        --process-settings 'h_ggf_4l,unstack,color=#000000,scale=10'
        --plot-suffix 'customized'
        --skip-ratio
        "$@"
    )

    law run cf.PlotVariables1D "${args[@]}"
}

action "$@"

